import java.lang.*;
import java.util.*;
import java.io.*;

class Worker implements Runnable {
    public int threadId;
    public int numThreads;
    public long totalSize;
    public long blockSize;
    public long numWords;
    public String filename;

    public Worker(int threadId, int numThreads, long totalSize, String filename) {
        this.threadId = threadId;
        this.numThreads = numThreads;
        this.blockSize = 8 * 1000;
        this.totalSize = totalSize;
        this.filename = filename;
        this.numWords = 0;
    }

    public long countWords(String buffer) {
        long numWords;
        String token;
        StringTokenizer tokenizer;

        numWords = 0;
        tokenizer = new StringTokenizer(buffer, " /\n");
        while (tokenizer.hasMoreTokens()) {
            token = tokenizer.nextToken();
            numWords += 1;
        }

        return numWords;
    }

    public void run() {
        byte[] buffer = new byte[8 * 1000];
        long offset;
        RandomAccessFile infil;
        String line;

        try {
            infil = new RandomAccessFile(filename, "r");
            offset = blockSize * threadId;
            do {
                infil.seek(offset);
                infil.readFully(buffer);
                numWords += countWords(new String(buffer));
                offset += blockSize * numThreads;
            } while (offset < totalSize);
            infil.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

public class ParseBmark {
    static int maxNumThreads = 8;
    //static long totalSize = 400L * 1000 * 1000 * 1000;
    static long totalSize = 400L * 1000 * 1000 * 10;

    public static void benchmark(int numThreads, String filename) {
        ArrayList<Thread> threads = new ArrayList<Thread>();
        ArrayList<Worker> workers = new ArrayList<Worker>();
        long start, end, execTime, numWords;

        for (int i = 0; i < numThreads; i++) {
            workers.add(new Worker(i, numThreads, totalSize, filename));
            threads.add(new Thread(workers.get(i)));
        }

        start = System.currentTimeMillis();
        for (int i = 0; i < numThreads; i++) {
            threads.get(i).start();
        }

        numWords = 0;
        for (int i = 0; i < numThreads; i++) {
            try {
                threads.get(i).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            numWords += workers.get(i).numWords;
        }
        end = System.currentTimeMillis();
        execTime = ((long) (end - start)) / 1000;

        System.out.println("Benchmark -- " + numThreads + " thread(s):");
        System.out.println("-> elapsed time: " + execTime + " s");
        System.out.println("-> throughput: " + numWords / execTime + " words/s (tokens/s)");
        System.out.println("-> throughput: " + (totalSize / 1000000) / execTime + " MB/S");
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Please pass only the input file argument!");
            System.exit(-1);
        }

        for (int i = 1; i <= maxNumThreads; i *= 2) {
            benchmark(i, args[0]);
        }
    }
}
