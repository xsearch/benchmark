#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include <fcntl.h>
#include <unistd.h>

#define DEFAULT_NUM_THREADS 1
#define DEFAULT_BLOCK_SIZE 1024
#define DEFAULT_NUM_BLOCKS 0L

struct args_t {
    char *file_path;
    int thread_id;
    int num_threads;
    int block_size;
    long num_blocks;
    long total_size;
    long *offset;
    pthread_spinlock_t *spin;
} args_t;

void *work(void *work_args)
{
    char *buffer;
    int fd;
    long rc, rd, offset;
    struct args_t *args;

    args = (struct args_t*) work_args;
    buffer = (char *) malloc(sizeof(char) * args->block_size);

    fd = open(args->file_path, O_RDONLY);
    if (fd <= 0) {
        printf("Error reading file in thread %d!\n", args->thread_id);
        pthread_exit(NULL);
    }
    
    rc = posix_fadvise(fd, 0, 0, POSIX_FADV_NOREUSE);
    if (rc != 0) {
        printf("Error while advising kernel!\n");
    }

    pthread_spin_lock(args->spin);
    offset = *(args->offset);
    *(args->offset) += (long) args->block_size;
    pthread_spin_unlock(args->spin);

    while (offset < args->total_size) {
        rc = 0;
        while (rc < args->block_size) {
            rd = pread(fd, &buffer[rc], args->block_size - rc, offset + rc);
            if (rd == 0) {
                break;
            }
            rc += rd;
        }
        
        rc = 0;
        while (rc < args->block_size) {
            rd = pread(fd, &buffer[rc], args->block_size - rc,
                    offset + args->block_size + rc);
            if (rd == 0) {
                break;
            }
            rc += rd;
        }
        pthread_spin_lock(args->spin);
        offset = *(args->offset);
        *(args->offset) += (long) args->block_size;
        pthread_spin_unlock(args->spin);
    }

    close(fd);
    free(buffer);

    pthread_exit(NULL);
}

void benchmark(
        char *file_path,
        int num_threads,
        int block_size,
        long num_blocks)
{
    int rc, i;
    long total_size, offset;
    double exec_time;
    pthread_t *threads;
    pthread_spinlock_t spin;
    struct args_t *args;
    struct timeval start, end;

    total_size = num_blocks * block_size * num_threads;
    threads = (pthread_t*) malloc(sizeof(pthread_t) * num_threads);
    args = (struct args_t*) malloc(sizeof(struct args_t) * num_threads);

    offset = 0;
    pthread_spin_init(&spin, PTHREAD_PROCESS_PRIVATE);
    for (i = 0; i < num_threads; i++) {
        args[i].file_path = file_path;
        args[i].thread_id = i;
        args[i].num_threads = num_threads;
        args[i].block_size = block_size;
        args[i].num_blocks = num_blocks;
        args[i].total_size = total_size;
        args[i].offset = &offset;
        args[i].spin = &spin;
    }

    gettimeofday(&start, NULL);
    for (i = 0; i < num_threads; i++) {
        rc = pthread_create(&threads[i], NULL, work, (void *) &args[i]);
        if (rc) {
            printf("Error creating thread %d!\n", i);
        }
    }

    for (i = 0; i < num_threads; i++) {
        rc = pthread_join(threads[i], NULL);
        if (rc) {
            printf("Error joining thread %d!\n", i);
        }
    }
    gettimeofday(&end, NULL);

    exec_time = (((double) end.tv_sec - (double) start.tv_sec) * 1000000
            + ((double) end.tv_usec - (double) start.tv_usec)) / 1000000;

    printf("Benchmark complete -- (results)\n"
        "block size (in bytes): %d\n"
        "number of threads:     %d\n"
        "total size (in bytes): %ld\n"
        "elapse time:           %lf sec\n"
        "throughput:            %lf MB/s\n",
        block_size,
        num_threads,
        total_size,
        exec_time,
        (double) (total_size / 1000000) / exec_time);
    
    pthread_spin_destroy(&spin);
    free(args);
    free(threads);
}

int parse_args( char *file_path, int *num_threads, int *block_size,
        long *num_blocks, int argc, char **argv)
{
    int i, len;
    char *buffer;

    if (argc == 1 || argc % 2 != 0) {
        printf("Usage: ./bin/read-bmark.elf [OPTION] FILE_PATH\n\n"
            "OPTION:\n"
            "  -t [number of threads]    How many threads to use.\n"
            "  -b [block size]           The size of the block to read. "
            "Units are B,K,M.\n"
            "  -n [number of blocks]     How many blocks per thread.\n");
        return -1;
    }

    for (i = 1; i < argc - 1; i += 2) {
        if (strcmp(argv[i], "-t") == 0) {
            *num_threads = atoi(argv[i + 1]);
            if (*num_threads <= 0) {
                printf("Incorrect value for number of threads!\n");
                return -2;
            }
        } else if (strcmp(argv[i], "-b") == 0) {
            len = strlen(argv[i + 1]);
            if (argv[i + 1][len - 1] == 'B') {
                buffer = (char*) malloc(sizeof(char) * len);
                strcpy(buffer, argv[i + 1]);
                buffer[len - 1] = '\0';
                *block_size = atoi(buffer);
                free(buffer);
            } else if (argv[i + 1][len - 1] == 'K') {
                buffer = (char*) malloc(sizeof(char) * len);
                strcpy(buffer, argv[i + 1]);
                buffer[len - 1] = '\0';
                *block_size = atoi(buffer) * 1024;
                free(buffer);
            } else if (argv[i + 1][len - 1] == 'M') {
                buffer = (char*) malloc(sizeof(char) * len);
                strcpy(buffer, argv[i + 1]);
                buffer[len - 1] = '\0';
                *block_size = atoi(buffer) * 1024 * 1024;
                free(buffer);
            } else {
                printf("Incorrect value for the block size!\n");
                return -2;
            }
            if (*block_size <= 0) {
                printf("Incorrect value for the block size!\n");
                return -2;
            }
        } else if (strcmp(argv[i], "-n") == 0) {
            *num_blocks = atol(argv[i + 1]);
            if (*num_blocks <= 0) {
                printf("Incorrect number of blocks!\n");
                return -2;
            }
        }
    }

    strcpy(file_path, argv[argc - 1]);

    return 0;
}

int main(int argc, char **argv)
{
    char file_path[4096];
    int rc, num_threads, block_size;
    long num_blocks;

    num_threads = DEFAULT_NUM_THREADS;
    block_size = DEFAULT_BLOCK_SIZE;
    num_blocks = DEFAULT_NUM_BLOCKS;

    rc = parse_args(file_path, &num_threads, &block_size, &num_blocks, argc,
            argv);
    
    if (rc != 0) {
        return -1;
    }

    benchmark(file_path, num_threads, block_size, num_blocks);

    return 0;
}
