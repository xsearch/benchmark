#!/bin/bash

file_dir_in=""
file_dir_out=""
mkdir -p $file_dir_out
for dtype in "blocks"
do
    for stype in "para" "mutex" "spin" "atomic"
    do
        log_file="bmark-read-$dtype-$stype"
        for btype in "1K" "4K" "16K"
        do
            ./scripts/extract.py "$file_dir_in/$log_file-$btype.1.log" \
                "$file_dir_in/$log_file-$btype.2.log" \
                "$file_dir_in/$log_file-$btype.3.log" \
                &>> "$file_dir_out/$log_file-$btype.dat"
        done
    done
done

