#!/usr/bin/python3

import sys

def get_data(filein):
    numthreads = []
    throughput = []

    for line in filein:
        if 'number of threads:' in line:
            trline = line.replace(' ', '').replace('\n', '')
            elem = trline.split(':')[1]
            numthreads.append(int(elem))
        if 'throughput:' in line:
            trline = line.replace(' ', '').replace('\n', '')
            elem = trline.replace('MB/s', '').split(':')[1]
            throughput.append(float(elem))

    return (numthreads, throughput)

def main(args):
    numthreads = []
    throughput = []
    numfiles = 0

    if len(args[1:]) == 0:
        print("Give at least one file as an input!")
        return

    suma = lambda x, y: x + y
    norm = lambda x: x / numfiles
    for filename in args[1:]:
        with open(filename, "r") as filein:
            (numthreads_temp, throughput_temp) = get_data(filein)
            if len(numthreads) == 0:
                numthreads = numthreads_temp
            if len(throughput) == 0:
                throughput = throughput_temp
            else:
                throughput = list(map(suma, throughput, throughput_temp))
        numfiles += 1
    throughput = list(map(norm, throughput))
    
    line = ""
    for elem in numthreads:
        line += str(elem) + " "
    print(line)

    line = ""
    for elem in throughput:
        line += str(elem) + " "
    print(line)

if __name__ == "__main__":
    main(sys.argv)
