#!/usr/bin/python3

import sys
import matplotlib.pyplot as plt

def main(args):
    numthreads = []
    throughput = []

    maxthroughput = int(args[1])
    plotname = args[2]
    datstart = 3

    if len(args[datstart:]) == 0:
        print("Give at least one file as an input!")
        return

    for i in range(0, len(args[datstart:])):
        numthreads.append([])
        throughput.append([])
   
    i = 0
    for filename in args[datstart:]:
        with open(filename, "r") as filein:
            lineno = 0
            for line in filein:
                if lineno == 0:
                    numthreads_temp = line[:-2].split(" ")
                    numthreads[i] = [int(elem) for elem in numthreads_temp]
                    lineno = 1
                else:
                    throughput_temp = line[:-2].split(" ")
                    throughput[i] = [float(elem) for elem in throughput_temp]
                    lineno = 0
        i += 1
    
    if len(args[datstart:]) == 1:
        plt.ylim(0, maxthroughput)
        plt.yticks(range(0, maxthroughput + 1, 200))
        plt.plot(numthreads[0], throughput[0], 'go--')
        plt.xlabel("Number of threads")
        plt.ylabel("Throughput (MB/s)")
        plt.grid(True)
        plt.savefig(plotname, dpi=300)
    elif len(args[datstart:]) == 2:
        plt.ylim(0, maxthroughput)
        plt.yticks(range(0, maxthroughput + 1, 200))
        plt.plot(numthreads[0], throughput[0], 'go--',
                numthreads[1], throughput[1], 'bo--')
        plt.xlabel("Number of threads")
        plt.ylabel("Throughput (MB/s)")
        plt.grid(True)
        plt.savefig(plotname, dpi=300)
    elif len(args[datstart:]) == 3:
        plt.ylim(0, maxthroughput)
        plt.yticks(range(0, maxthroughput + 1, 200))
        plt.plot(numthreads[0], throughput[0], 'go--',
                numthreads[1], throughput[1], 'bo--',
                numthreads[2], throughput[2], 'co--')
        plt.xlabel("Number of threads")
        plt.ylabel("Throughput (MB/s)")
        plt.grid(True)
        plt.savefig(plotname, dpi=300)
    elif len(args[datstart:]) == 4:
        plt.ylim(0, maxthroughput)
        plt.yticks(range(0, maxthroughput + 1, 200))
        plt.plot(numthreads[0], throughput[0], 'go--',
                numthreads[1], throughput[1], 'bo--',
                numthreads[2], throughput[2], 'co--',
                numthreads[3], throughput[3], 'mo--')
        plt.xlabel("Number of threads")
        plt.ylabel("Throughput (MB/s)")
        plt.grid(True)
        plt.savefig(plotname, dpi=300)
    elif len(args[datstart:]) == 5:
        plt.ylim(0, maxthroughput)
        plt.yticks(range(0, maxthroughput + 1, 200))
        plt.plot(numthreads[0], throughput[0], 'go--',
                numthreads[1], throughput[1], 'bo--',
                numthreads[2], throughput[2], 'co--',
                numthreads[3], throughput[3], 'mo--',
                numthreads[4], throughput[4], 'ro--')
        plt.xlabel("Number of threads")
        plt.ylabel("Throughput (MB/s)")
        plt.grid(True)
        plt.savefig(plotname, dpi=300)
    elif len(args[datstart:]) == 6:
        plt.ylim(0, maxthroughput)
        plt.yticks(range(0, maxthroughput + 1, 200))
        plt.plot(numthreads[0], throughput[0], 'go--',
                numthreads[1], throughput[1], 'bo--',
                numthreads[2], throughput[2], 'co--',
                numthreads[3], throughput[3], 'mo--',
                numthreads[3], throughput[3], 'ro--',
                numthreads[3], throughput[3], 'yo--')
        plt.xlabel("Number of threads")
        plt.ylabel("Throughput (MB/s)")
        plt.grid(True)
        plt.savefig(plotname, dpi=300)

if __name__ == "__main__":
    main(sys.argv)
