#!/bin/bash

file_dir_in=""
file_dir_out=""
maxthroughput=3000
mkdir -p $file_dir_out
for dtype in "blocks"
do
    for stype in "para" "mutex" "spin" "atomic"
    do
        log_file="bmark-read-$dtype-$stype"
        ./scripts/plot.py $maxthroughput "$file_dir_out/$log_file.png" \
            "$file_dir_in/$log_file-1K.dat" \
            "$file_dir_in/$log_file-4K.dat" \
            "$file_dir_in/$log_file-16K.dat"
    done
    
    for btype in "1K" "4K" "16K"
    do
        log_file="bmark-read-$dtype"
        ./scripts/plot.py $maxthroughput "$file_dir_out/$log_file-$btype.png" \
            "$file_dir_in/$log_file-para-$btype.dat" \
            "$file_dir_in/$log_file-mutex-$btype.dat" \
            "$file_dir_in/$log_file-spin-$btype.dat" \
            "$file_dir_in/$log_file-atomic-$btype.dat"
    done
done

