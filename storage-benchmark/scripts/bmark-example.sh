#!/bin/bash

# legacy code
function clear_cache {
	sync
	sudo sh -c 'echo 3 > /proc/sys/vm/drop_caches'
}

file_size=2000000000
mkdir -p log

file_path=""
for dtype in "blocks"
do
    for stype in "para" "mutex" "spin" "atomic"
    do
        elf_path="bin/bmark-read-$dtype-$stype.elf"

        block_size=1000
        block_size_formatted=1K
        for lnum in 1 2 3
        do
            log_file="log/bmark-read-$dtype-$stype-1K.$lnum.log"
            echo -n "" > $log_file
            for num_threads in 1 2 4 6 8 12 16 24 32 48
            do
                # clear OS file system cache and buffers
                #clear_cache
                ./$elf_path -t $num_threads -b $block_size_formatted \
                    -n $((($file_size / $block_size) / $num_threads)) \
                    $file_path &>> $log_file
            done
        done

        block_size=4000
        block_size_formatted=4K
        for lnum in 1 2 3
        do
            log_file="log/bmark-read-$dtype-$stype-8K.$lnum.log"
            echo -n "" > $log_file
            for num_threads in 1 2 4 6 8 12 16 24 32 48
            do
                # clear OS file system cache and buffers
                #clear_cache
                ./$elf_path -t $num_threads -b $block_size_formatted \
                    -n $((($file_size / $block_size) / $num_threads)) \
                    $file_path &>> $log_file
            done
        done

        block_size=16000
        block_size_formatted=16K
        for lnum in 1 2 3
        do
            log_file="log/bmark-read-$dtype-$stype-16K.$lnum.log"
            echo -n "" > $log_file
            for num_threads in 1 2 4 6 8 12 16 24 32 48
            do
                # clear OS file system cache and buffers
                #clear_cache
                ./$elf_path -t $num_threads -b $block_size_formatted \
                    -n $((($file_size / $block_size) / $num_threads)) \
                    $file_path &>> $log_file
            done
        done

    done
done

