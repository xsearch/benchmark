#!/bin/bash

function clear_cache {
	sync
	sudo sh -c 'echo 3 > /proc/sys/vm/drop_caches'
}

export CLASSPATH="lib/lucene-7.1.0/build/core/classes/java/:bin"

datentime=$(date +'%Y-%m-%d-%H:%M')
path_hdd="/home/loki/data/"
path_ssd="/storage/data/"
terms="terms.txt"
metaterms="metaterms.txt"
log="iteration$datentime.log"

echo -n "" > $log

for i in {1..10}
do
    file="dataset$(($i * 200))MB.txt"

    clear_cache
    java XSearchData $path_hdd$file $path_hdd$terms &>> $log
done

for i in {1..10}
do
    file="dataset$(($i * 200))MB.txt"

    clear_cache
    java XSearchData $path_ssd$file $path_ssd$terms &>> $log
done

for i in {1..10}
do
    file="metaset$(($i * 50))kME.txt"

    clear_cache
    java XSearchMeta $path_hdd$file $path_hdd$metaterms &>> $log
done

for i in {1..10}
do
    file="metaset$(($i * 50))kME.txt"

    clear_cache
    java XSearchMeta $path_ssd$file $path_ssd$metaterms &>> $log
done
