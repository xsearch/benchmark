#!/bin/bash

if [ ! -d "download" ]
then
    mkdir download
fi

if [ ! -e "download/lucene-7.1.0-src.tgz" ]
then
    cd download
    wget http://www.namesdir.com/mirrors/apache/lucene/java/7.1.0/lucene-7.1.0-src.tgz
    cd ..
fi

if [ ! -d "lib" ]
then
    mkdir lib
fi

if [ ! -d "lib/lucene-7.1.0" ]
then
    tar xzvf download/lucene-7.1.0-src.tgz -C lib/
fi

dpkg -s openjdk-8-jdk &> /dev/null
if [ $? -eq 1 ]
then
    sudo apt install -y openjdk-8-jdk
fi

dpkg -s openjdk-8-jre &> /dev/null
if [ $? -eq 1 ]
then
    sudo apt install -y openjdk-8-jre
fi

dpkg -s ant &> /dev/null
if [ $? -eq 1 ]
then
    sudo apt install -y ant
fi

dpkg -s ivy &> /dev/null
if [ $? -eq 1 ]
then
    sudo apt install -y ivy
fi

if [ ! -e "lib/lucene-7.1.0/build/core/lucene-core-7.1.0-SNAPSHOT.jar" ]
then
    if [ ! -d "~/.ant/lib" ]
    then
        mkdir -p ~/.ant/lib
    fi
    
    cd lib/lucene-7.1.0
    ant ivy-bootstrap
    ant
    cd ../..
fi
