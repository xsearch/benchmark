#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#define GC_THREADS
#include <gc.h>

#define HASH_SIZE 0xFFFF

typedef struct hnode_t {
    struct dnode_t *dnode;
} hnode_t;

typedef struct dnode_t {
    unsigned int flen;
    char **terms;
    char **files;
} dnode_t;

typedef struct results_t {
    unsigned int len;
    char **files;
} results_t;

unsigned int ELFHash(char *str, unsigned int len)
{
    unsigned int hash = 0;
    unsigned int x = 0;
    unsigned int i = 0;

    for (i = 0; i < len; str++, i++) {
        hash = (hash << 4) + (*str);

        if ((x = hash & 0xF0000000L) != 0) {
            hash ^= (x >> 24);
        }

        hash &= ~x;
    }

    return (hash & 0x0000FFFF);
}

hnode_t *new_hashmap()
{
    hnode_t *hashmap;
    unsigned int i;

    hashmap = (hnode_t *) GC_MALLOC(HASH_SIZE * sizeof(hnode_t));
    for (i = 0; i < HASH_SIZE; i++) {
        hashmap[i].dnode = (dnode_t *) GC_MALLOC(sizeof(dnode_t));
        hashmap[i].dnode->flen = 0;
        hashmap[i].dnode->terms = NULL;
        hashmap[i].dnode->files = NULL;
    }

    return hashmap;
}

unsigned int update_zero(hnode_t *node, char *term, char *fname)
{
    unsigned int ok;
    dnode_t *old_dnode, *new_dnode;

    old_dnode = node->dnode;
    new_dnode = (dnode_t *) GC_MALLOC(sizeof(dnode_t));

    if (old_dnode->flen == 0) {
        new_dnode->flen = 1;
        new_dnode->terms = (char **) GC_MALLOC(sizeof(char *));
        new_dnode->files = (char **) GC_MALLOC(sizeof(char *));
        new_dnode->terms[0] = (char *) GC_MALLOC(strlen(term) * sizeof(char));
        new_dnode->files[0] = (char *) GC_MALLOC(strlen(fname) * sizeof(char));
        strcpy(new_dnode->terms[0], term);
        strcpy(new_dnode->files[0], fname);
        ok = __sync_bool_compare_and_swap(&node->dnode, old_dnode, 
                new_dnode);
        return ok;
    }
}

unsigned int exists(dnode_t *node, char *term, char *fname)
{
    unsigned int i;

    for (i = 0; i < node->flen; i++) {
        if (strcmp(node->files[i], fname) == 0
                && strcmp(node->terms[i], term) == 0) {
            return 1;
        }
    }

    return 0;
}

unsigned int update(hnode_t *node, char *term, char *fname)
{
    dnode_t *old_dnode, *new_dnode;
    unsigned int i, len, ok;

    old_dnode = node->dnode;
    new_dnode = (dnode_t *) GC_MALLOC(sizeof(dnode_t));

    if (!exists(old_dnode, term, fname)) {
        new_dnode->flen = old_dnode->flen + 1;
        len = new_dnode->flen;
        new_dnode->terms = (char **) GC_MALLOC(len * sizeof(char *));
        new_dnode->files = (char **) GC_MALLOC(len * sizeof(char *));
        for (i = 0; i < old_dnode->flen; i++) {
            new_dnode->terms[i] = old_dnode->terms[i];
            new_dnode->files[i] = old_dnode->files[i];
        }
        new_dnode->terms[len - 1] = (char *) GC_MALLOC(strlen(term) 
                * sizeof(char));
        new_dnode->files[len - 1] = (char *) GC_MALLOC(strlen(fname) 
                * sizeof(char));
        strcpy(new_dnode->terms[len - 1], term);
        strcpy(new_dnode->files[len - 1], fname);
        ok = __sync_bool_compare_and_swap(&node->dnode, old_dnode, 
                new_dnode);
        return ok;
    }

    return 1;
}

void insert(hnode_t *hashmap, char *term, char *filename)
{
    unsigned int key, ok;

    key = ELFHash(term, strlen(term));

    if (hashmap[key].dnode == NULL) {
        //printf("NULL dnode\n");
        return;
    }
        
    do {
        if (hashmap[key].dnode->flen == 0) {
            ok = update_zero(&hashmap[key], term, filename);
        } else {
            if (!exists(hashmap[key].dnode, term, filename)) {
                ok = update(&hashmap[key], term, filename);
            } else {
                ok = 1;
            }
        }
    } while (!ok);
}

results_t *lookup(hnode_t *hashmap, char *term)
{
    results_t *results;
    unsigned int key, i, j, len;
    
    key = ELFHash(term, strlen(term));

    results = (results_t *) GC_MALLOC(sizeof(results_t));
    results->len = 0;
    results->files = NULL;
    for (i = 0; i < hashmap[key].dnode->flen; i++) {
        if (strcmp(hashmap[key].dnode->terms[i], term) == 0) {
            results->len += 1;
            len = results->len;
            results->files = (char **) GC_REALLOC(results->files, 
                    len * sizeof(char *));
            results->files[len - 1] = (char *) GC_MALLOC(
                    strlen(hashmap[key].dnode->files[i]) * sizeof(char));
            strcpy(results->files[len - 1], hashmap[key].dnode->files[i]);
        }
    }

    return results;
}

void print_hashmap(hnode_t *hashmap)
{
    unsigned int i, j;

    for (i = 0; i < HASH_SIZE; i++) {
        if (hashmap[i].dnode->flen > 0) {
            for (j = 0; j < hashmap[i].dnode->flen; j++) {
                if (hashmap[i].dnode->terms[j] != NULL) {
                    printf("%d %s\n", i, hashmap[i].dnode->terms[j]);
                } else {
                    printf("bad pointer\n");
                }
            }
        }
    }
}
