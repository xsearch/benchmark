%{
#include <stdio.h>
#include <string.h>
    
enum yytoktype {
    WORD = 1,
};

char *word;

%}

%option reentrant
%option noyywrap

%%

[a-zA-Z0-9]+    { word = strdup(yytext); return WORD; }
"\n"            {}
.               {}

%%

char *simtok(char *input, yyscan_t scanner)
{
    int tok;

    if (input != NULL) {
        yy_scan_string(input, scanner);
    }

    tok = yylex(scanner);
    if (tok == WORD) {
        if (strcmp(word, "") != 0) {
            return word;
        } else {
            return NULL;
        }
    } else {
        return NULL;
    }
}

