#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>

#define GC_THREADS
#include <gc.h>

#include "simtok.h"
#include "hashmap.h"

#define NUM_THREADS 4
#define BLOCK_SIZE 8 * 1000 * 1000

typedef struct thread_t {
    int tid;
    int num_threads;
    //int num_files;
    hnode_t *hashmap;
    char *files;
} thread_t;

void *parallel_index(void *thread_arg)
{
    thread_t *arg;
    char buffer[BLOCK_SIZE], *tok, term[1024];
    long seek;
    //int i;
    FILE *fil;
    yyscan_t scanner;

    arg = (thread_t *) thread_arg;
    yylex_init(&scanner);
    //for (i = 0; i < arg->num_files; i++) {
            //printf("file %s starting!\n", arg->files);
        seek = arg->tid * BLOCK_SIZE;
        fil = fopen(arg->files, "r");
        fseek(fil, seek, 0);
        while (fread(buffer, sizeof(char), BLOCK_SIZE, fil) > 0) {
            tok = simtok(buffer, scanner);
            while(tok != NULL) {
                strcpy(term, tok);
                insert(arg->hashmap, term, arg->files); 
                tok = simtok(NULL, scanner);
            }
            seek += arg->num_threads * BLOCK_SIZE;
            fseek(fil, seek, 0);
        }
        fclose(fil);
        //if (arg->tid == 0) {
            //printf("file %s done!\n", arg->files);
        //}
    //}
    yylex_destroy(scanner);
}

int main(int argc, char **argv)
{
    FILE *fil;
    char buffer[2048], **files;
    int n, m, i, j;
    hnode_t *hashmap;
    pthread_t threads[NUM_THREADS];
    thread_t args[NUM_THREADS];
    int rc;
    results_t *results;
    struct timeval start, end;
    long indexTime, indexSize, searchTime;

    GC_INIT();

    gettimeofday(&start, NULL);
    n = 0;
    m = 50;
    files = (char **) GC_MALLOC(m * sizeof(char *));

    fil = fopen(argv[1], "r");
    while (fgets(buffer, 2048, fil) != NULL) {
        files[n] = (char *) GC_MALLOC(strlen(buffer) * sizeof(char));
        strcpy(files[n], buffer);
        files[n][strlen(buffer) - 1] = '\0';
        n++;

        if (n >= m) {
            m += m;
            files = (char **) GC_REALLOC(files, m * sizeof(char *));
        }
    }
    fclose(fil);

    //hashmap = (hnode_t **) GC_MALLOC(n / 2 * sizeof(hnode_t *));
    //for (i = 0; i < n / 2; i++) {
    //    hashmap[i] = new_hashmap();
    //}
    hashmap = new_hashmap();

    for (j = 0; j < n; j++) {
    //***********************
    for (i = 0; i < NUM_THREADS; i++) {
        args[i].tid = i;
        args[i].num_threads = NUM_THREADS;
        //args[i].num_files = n;
        args[i].hashmap = hashmap;
        args[i].files = files[j];
    }

    for (i = 0; i < NUM_THREADS; i++) {
        rc = pthread_create(&threads[i], NULL, parallel_index, &args[i]);
        if (rc) {
            printf("Could not create a thread!\n");
        }
    }

    for (i = 0; i < NUM_THREADS; i++) {
        rc = pthread_join(threads[i], NULL);
        if (rc) {
            printf("Could not join a thread!\n");
        }
    }
    GC_gcollect();
    //***********************
    }
    gettimeofday(&end, NULL);
    indexTime = (((long) end.tv_sec - (long) start.tv_sec) 
            * 1000000 + (end.tv_usec - start.tv_usec)) / 1000;

    //GC_gcollect();
    indexSize = GC_get_heap_size();

    gettimeofday(&start, NULL);
    fil = fopen(argv[2], "r");
    while (fgets(buffer, 2048, fil) != NULL) {
        buffer[strlen(buffer) - 1] = '\0';
        results = lookup(hashmap, buffer);
        //for (i = 0; i < results->len; i++) {
        //    printf("%s\n", results->files[i]);
        //}
    }
    fclose(fil);
    gettimeofday(&end, NULL);
    searchTime = (((long) end.tv_sec - (long) start.tv_sec) 
            * 1000000 + (end.tv_usec - start.tv_usec)) / 1000;
    
    printf("IndexTime: %ld\n", indexTime);
    printf("IndexSize: %ld\n", indexSize);
    printf("SearchTime: %ld\n", searchTime);
    
    return 0;
}
