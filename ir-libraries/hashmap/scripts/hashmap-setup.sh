#!/bin/bash

if [ ! -d "download" ]
then
    mkdir download
fi

if [ ! -e "download/gc-7.2g.tar.gz" ]
then
    cd download
    wget http://www.hboehm.info/gc/gc_source/gc-7.2g.tar.gz
    cd ..
fi

if [ ! -d "download/gc-7.2" ]
then
    cd download
    tar xvf gc-7.2g.tar.gz
    cd ..
fi

if [ ! -d "lib" ]
then
    mkdir lib
fi

dpkg -s flex &> /dev/null
if [ $? -eq 1 ]
then
    sudo apt install -y flex
fi

if [ ! -d "lib/gc-7.2" ]
then
    topdir=$(pwd)
    cd download/gc-7.2
    ./configure --prefix=$topdir/lib/gc-7.2 \
        --enable-threads=posix \
        --enable-thread-local-alloc \
        --enable-parallel-mark
    make
    make check
    make install
    cd ../..
fi
