#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define COUNT1 0x5555555555555555
#define COUNT2 0x3333333333333333
#define COUNT3 0x0F0F0F0F0F0F0F0F
#define COUNT4 0x00FF00FF00FF00FF
#define COUNT5 0x0000FFFF0000FFFF
#define COUNT6 0x00000000FFFFFFFF

typedef struct files_t {
    char *filepath;
    files_t *next;
} files_t;

typedef struct node_t {
    pthread_mutex_t mtx_node;
    pthread_mutex_t mtx_files;
    long bitmap;
    int inserted;
    int branch_len;
    node_t *branch;
    files_t *files;
}

typedef

void insert_file(files_t *files, char *filename) {
    files_t *p;
    
    p = files;
    while (p->next != NULL) {
        p = p->next;
    }

    p->next = malloc(sizeof(files_t));
    p = p->next;
    p->filepath = strdup(filename);
    p->next = NULL;
}

int bitcount(long bitmap) {
    bitmap = ((bitmap >> 1) & COUNT1) + (bitmap & COUNT1);
    bitmap = ((bitmap >> 2) & COUNT2) + (bitmap & COUNT2);
    bitmap = ((bitmap >> 4) & COUNT3) + (bitmap & COUNT3);
    bitmap = ((bitmap >> 8) & COUNT4) + (bitmap & COUNT4);
    bitmap = ((bitmap >> 16) & COUNT5) + (bitmap & COUNT5);
    bitmap = ((bitmap >> 32) & COUNT6) + (bitmap & COUNT6);
    return (int) bitmap;
}

void insert(node_t root, char *term, char *filename)
{
    long bit = 1;
    int pos, i;
    node_t *new_branch, next_branch;
    
    if (strlen(term) == 0) {
        return;
    }

    if (term[0] == '\0') {
        if (!root.inserted) {
            pthread_mutex_lock(&root.mtx_files);
            if (root.inserted) {
                pthread_mutex_unlock(&root.mtx_files);
                return;
            }
            if(root.files == NULL) {
                root.files = malloc(sizeof(files_t));
                root.files->filepath = strdup(filename);
                root.files->next = NULL;
            } else {
                insert_file(root.files, filename);
            }
            root.inserted = 1;
            pthread_mutex_unlock(&root.mtx.files);
        }
    }

    if (term[0] >= 'a' && term[0] <= 'z') {
        bit <<= (term[0] - 'a');
    } else if (term[0] >= 'A' && term[0] <= 'Z') {
        bit <<= (26 + term[0] - 'A');
    } else if (term[0] >= '0' && term[0] <= '9') {
        bit <<= (52 + term[0] - '0');
    } else {
        return;
    }

    if ((bit & root.bitmap) != 0) {
        pthread_mutex_lock(&root.mtx_node);
        pos = bitcount((bit - 1) & root.bitmap);
        next_branch = root.branch[pos];
        pthread_mutex_unlock(&root.mtx_node);
        insert(next_branch, &term[1], filename);
    } else {
        pthread_mutex_lock(&root.mtx_node);
        if ((bit & root.bitmap) != 0) {
            pthread_mutex_lock(&root.mtx_node);
            pos = bitcount((bit - 1) & root.bitmap);
            next_branch = root.branch[pos];
            pthread_mutex_unlock(&root.mtx_node);
            insert(next_branch, &term[1], filename);
            return;
        }
        bitmap |= bit;
        branch_len += 1;
        pos = bitcount((bit - 1) & root.bitmap);
        new_branch = malloc(sizeof(node_t));
        for (i = 0; i < branch_len; i++) {
            if (i == pos) {
                continue;
            }
            new_branch[i] = root.branch[i];
        }
        free(root.branch);
        root.branch = new_branch;
        pthread_mutex_init(&(root.branch[pos].mtx_node));
        pthread_mutex_init(&(root.branch[pos].mtx_files));
        root.branch[pos].bitmap = 0;
        root.branch[pos].inserted = 0;
        root.branch[pos].branch_len = 0;
        root.branch[pos].branch = NULL;
        root.branch[pos].files = NULL;
        next_branch = root.branch[pos];
        pthread_mutex_unlock(&root.mtx_node);
        insert(next_branch, &term[1], filename);
    }
}

files_t *lookup(node_t root, *term)
{
    long bit;
    int pos;

    if (term[0] == '\0') {
        return root.files;
    }

    if (term[0] >= 'a' && term[0] <= 'z') {
        bit <<= (term[0] - 'a');
    } else if (term[0] >= 'A' && term[0] <= 'Z') {
        bit <<= (26 + term[0] - 'A');
    } else if (term[0] >= '0' && term[0] <= '9') {
        bit <<= (52 + term[0] - '0');
    } else {
        return NULL;
    }

    if ((bit & root.bitmap) != 0) {
        pos = bitcount((bit - 1) & root.bitmap);
        return lookup(root.branch[pos], &term[1]);
    } else {
        return NULL;
    }
}

void work(void *args)
{

}

int main(int argc, char **argv)
{
    
    return 0;
}
