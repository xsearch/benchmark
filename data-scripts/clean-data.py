import sys

def clean(infilename):
    infil = open(infilename, "r")
    outfil = open(infilename + ".clean", "w")
    
    for line in infil:
        buff = ""
        num = 0
        for elem in line.split(" "):
            if len(elem) > 0:
                if num == 0:
                    buff += elem
                elif num < 16:
                    buff += " " + elem
                elif num == 16:
                    buff += " " + elem.replace("%2F", "/")
                if num > 16:
                    buff += "_" + elem.replace("%2F", "/")
                num += 1
        outfil.write(buff)

    infil.close()
    outfil.close()

if __name__ == "__main__":
    for infilename in sys.argv[1:]:
        clean(infilename)
        print(infilename + " ... cleaned!")
