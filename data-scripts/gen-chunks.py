import sys

def generate(filename, filepath):
    counter = 0
    file_num = 1
    filepath = filepath + "/metadata"
    infil = open(filename, "r")
    outfil = open(filepath + str(file_num) + ".dat", "w")
    for line in infil:
        if counter == 1000:
            outfil.close()
            file_num += 1
            if file_num >= 1000000:
                break
            outfil = open(filepath + str(file_num) + ".dat", "w")
            counter = 0
        outfil.write(line)
        counter += 1
    outfil.close()
    infil.close()

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Please pass only the input file and output path arguments!")
        sys.exit(-1)

    generate(sys.argv[1], sys.argv[2])
